/** \file pdp1.h
* \brief Defines many macro functions and values, and declares most variables and many functions.*/
//Variables in docs not found in code:
//  Emax (Emin was found, called emin)
//  vx0L, vx0R, vxtL, vxtR, vxcL, vxcR

#ifndef __pdp1_h
#define __pdp1_h

#include <math.h>
#include <stdio.h>
//#include <unistd.h>
#include <stdlib.h>

#define max(x, y)       (((x) > (y)) ? (x) : (y))
#define min(x, y)       (((x) < (y)) ? (x) : (y))
#define sqr(a)                           ((a)*(a))
#ifdef linux
  #define cosd(d) (cos(M_PI*d/180))
  #define sind(d) (sin(M_PI*d/180))
#endif

#define EPS0    8.8542e-12      ///<Vacuum permittvity, in farads per meter (F/m).
#define NperTORR  8.3221e20
#define NSMAX     7             ///<Maximum allowed nb. of particle species.
//#define HISTMAX   32768         ///<Size of history arrays.
#define HISTMAX   1048576         ///<Size of history arrays. Increased to avoid potentially unfixed segfault.

#define DBL_MIN   1E-200 //TODO: Consider replacing with C library DBL_MIN.
#define FLOAT_MIN   1e-4 //TODO: Consider replacing with C library FLOAT_MIN.
#define DEN_MIN   1
#define True            1
#define False           0
#define M_PI    3.14159265358979323846
#define onesixth 0.1666666667 ///<1/6
#define one24 0.041666667     ///<1/24
#define one12 0.083333333     ///<1/12
#define onethird 0.333333333  ///<1/3
#define twothirds 0.666666667 ///<2/3

#define BIT_REVERSE     1
#define RANDOM          0

/*  PG, attempt to read the gas type from input deck */
#define HELIUM 1
#define ARGON 2
#define NEON 3
#define OXYGEN 4
#define MCC 5 //If gas is set to MCC, old cross-section parameters are used.

#define USE_RFA 1 ///<If 0, removes the RFA circuit, and uses the circuit specified in the input file.

/*******************************************************************/

float        nc2p,                ///<Nb. of physical particles per computer super-particle.
             length,              ///<Length of the system in meters - distance between the electrodes.
             area,                ///<Area of an electrode, in m^2.
             rhoback,             ///<Fixed background non-accelerating charge density, in C/m^3.
             backj,               ///<Background current density, in A/m^2.
             dde,                 ///<Sinusoidal perturbation of charge density (dx/l) at t=0: dx(x) = l*dde*sin(2*Pi*x/l)
             epsilon,             ///<Initially the background dielectricity, is later set to epsilon*epsilon_0.
             b,                   ///<Applied homogeneous magnetic field, in teslas.
             psi,                 ///<Angle between the magnetic field (on the x-z plane) and the normal of the electrodes (the x axis), in degrees.
             extr,                ///<External circuit resistance, in ohms.
             extl,                ///<External circuit inductance, in henries.
             extc,                ///<External circuit capacitance, in farads.
             w0,                  ///<2Pi*frequency of AC source, in Hz.
             dcbias,              ///<Height of the DC ramp, in volts or amperes.
             acbias,              ///<Amplitude of the AC source, in volts or amperes.
             extq,                ///<External capacitor charge.
             extq_1, extq_2, extq_3,
             exti,
             sigma,               ///<LHS plate surface charge density.
             oldsigma,            ///<LHS plate surface charge density in the previous tick.
             dx,                  ///<Width of a spatial cell, in meters.
             dt,                  ///<Time step, in seconds.
             vxscale, vscale,
             xnc,                 ///<Nb. of spatial cells.
             pressure,            ///<Background neutral gas pressure, in torr (1 torr = 133.3224 Pa).
             gtemp,               ///<Background neutral gas thermal temperature, in eV.
             ramp,                ///<Rate of ramping for voltage or current source, in V/s or A/s.
             theta0,              ///<Initial phase angle of AC source.
             risetime,            ///<Time required for the ramped current or voltage source to reach its final value, in s.
             seec[NSMAX],         ///<Secondary electron emission coefficient.
                                  /**<If seec[isp] set to say 0.1, on average 1 electron is injected for every 10 incident particles of species isp.*/
             jwall[NSMAX],        ///<Accumulates current surface density deposited by plasma on LHS plate each tick, in A/m^2.
             jnorm[NSMAX],        ///<Used as a scaling factor. Equals computer_particle_charge/scaling_factor(usually 1)/dt/plate_area, and is thus a unit of current surface density.
             q[NSMAX],            ///<Initially charge per physical particle, in coulumbs, but gets multiplied with nc2p/area, so becomes computer_particle_charge/plate_area.
             m[NSMAX],            ///<Mass per physical particle, in kg.
             qm[NSMAX],           ///<Charge/Mass ratio for particle.
             jj0[NSMAX][2],       ///<jj0[isp][0] is the injected current density of species isp from the left electrode, and jj0[isp][1] from the right electrode, in A/m^2.
             v0[NSMAX][2],
             vt[NSMAX][2],        ///<vt[species][0] is the thermal velocity for particles injected from the left wall, and vt[species][1] for the right wall.
                                  /**<Thermal velocity here is taken to mean v_thermal = sqrt(k_Boltzmann * T / m)
                                  *   and the injected particle velocity is v = drift + abs(maxwell_distribution(mean=0, stdev=v_thermal))
                                  *   vx_average injected for 0 drift thus equals v_thermal/sqrt(2Pi) */
             vc[NSMAX][2],
             v0y[NSMAX],          ///<Particle drift velocity in the y direction, in m/s.
             vty[NSMAX],          ///<Particle thermal velocity in the y direction, in m/s.
             v0z[NSMAX],          ///<Particle drift velocity in the z direction, in m/s.
             vtz[NSMAX],          ///<Particle thermal velocity in the z direction, in m/s.
             tx[NSMAX], tz[NSMAX], sx[NSMAX], sz[NSMAX],
             emin[NSMAX],         ///<Min. energy seen in the energy distribution diagnostic of the species at left wall/in the system, in eV.
             de[NSMAX],
             dtheta[NSMAX],
             enter[NSMAX][2],     ///<enter[isp][0] is the nb. of particles of species isp injected each "dt" from the left electrode, and enter[isp][1] from the right.
             emin_mid[NSMAX], de_mid[NSMAX],
             xs_mid[NSMAX],       ///<Left boundary of region over which the energy distribution is calculated (XStart in doc).
             xf_mid[NSMAX],       ///<Right boundary of region over which the energy distribution is calculated (XFinish in doc).
             a_scale[NSMAX],
             sp_chi_scale[NSMAX], ///<Some kind of species-specific susceptibility scale factor. Is set to 0 for the explicit solver (if imp_flag == 0).
             Escale[NSMAX], jdote_scale[NSMAX], j_scale[NSMAX],

             rfa_left_grid_pos,       ///<Scaled position of the left retarding field analyzer (RFA) grid on the x-axis.
             rfa_left_grid_opacity,   ///<Probability that a particle crossing the left RFA grid will collide with it.
             rfa_left_grid_jp[NSMAX], ///<Accumulates current density of each particle species on left RFA grid each tick.
             rfa_left_grid_jp_total,  ///<The total plasma current density flowing to the left RFA grid.
             rfa_left_grid_sigma,     ///<Left RFA grid surface charge density.
             rfa_left_grid_oldsigma,  ///<Left RFA grid surface charge density in the previous tick.

             rfa_right_grid_pos,      ///<Scaled position of the right retarding field analyzer (RFA) grid on the x-axis.
             rfa_right_grid_opacity,  ///<Probability that a particle crossing the right RFA grid will collide with it.
             rfa_right_grid_jp[NSMAX],///<Accumulates current density of each particle species on right RFA grid each tick.
             rfa_right_grid_jp_total, ///<The total plasma current density flowing to the right RFA grid.
             rfa_right_grid_sigma,    ///<Right RFA grid surface charge density.
             rfa_right_grid_oldsigma, ///<Right RFA grid surface charge density in the previous tick.

             U_lgrid_min,             ///<Lower bound for voltage U_1 stepwise oscillation.
             U_lgrid_max,             ///<Upper bound for voltage U_1 stepwise oscillation.
             U_lwall,                 ///<Voltage across voltage source nb 0, applied to LHS plate.
             U_1,                     ///<Voltage across voltage source nb 1, conditionally applied to left RFA grid.
             U_2,                     ///<Voltage across voltage source nb 2, conditionally applied to right RFA grid.

             edist_e_min,             ///<Minimum energy for the energy histogram, in eV.
             edist_e_max,
             edist_area_0_start,
             edist_area_0_end,
             edist_area_1_start,
             edist_area_1_end;

int          nsp,           ///<Nb. of particle species. Must be <= NSMAX.
             nc,            ///<Nb. of spatial cells. Cell width = length/nc.
             ng,            ///<Nb. of spatial cells + 1. Most arrays (E-field, charge density..) have ng entries - they measure values at the borders of cells.
             secondary,     ///<Secondary electron emission flag (0=off, 1=species 1, etc.)
                            /**<The emitted electron species give the emitted velocity distribution
                            at the surface specified for the species (see SPECIES PARAMETERS).*/
             ionspecies,    ///<Ion species created by electron-neutral ionization collisions (2=created ions are of type species 2, etc.)
                            /**<It is enumerated starting at 1, so, for example, positions of the ionspecies are stored in x[ionspecies-1].
                            \note This also specifies the type of background neutral gas particles colliding with electrons.*/
             ecollisional,  ///<Flag for ionization, elastic, and excitation electron-neutral collisions (0=off, 2=species 2 is the colliding ion species, etc.)
                            /**<\note Only ONE species can be the colliding electron species.*/
             icollisional,  ///<Flag for scattering and charge exchange ion-neutral collisions (0=off, 2=species 2 is the collidiing ion species, etc.)
                            /**<\note Only ONE species can be the colliding ion species.*/
             hist_hi, thist_hi, freq_hi, interval,
             nsmoothing,    ///<Nb. of times a (1,2,1) digital smoothing filter is applied to the charge density arrays prior to the field-solve.
             ntimestep,     ///<Read from input file, but otherwise unused.
             nfft,          ///<Nb. of samples for Fast Fourier Transf. analyzer (must be 2^n). If 0, no FFT analysis is done, and frequency domain diagnostics are not shown.
             n_ave,         ///<Nb. of samples for for the average diagnostics. If 0, no averages are done or shown.
             dcramped,      ///<Flag for ramping external voltage/current source to a final DC value (0=no, 1=yes).
             reflux,        ///<Flag for refluxing the particles at the right wall (0=off, 1=on).
                            /**<If on, particles hitting the right wall are not absorbed, but are reflected back into the system.
                            Since in this case the right wall does not charge up, it serves only as a symmetry plane allowing
                            for a semi-infinite plasma at the right wall. Particles of each species are refluxed at the
                            temperature specified for the species.*/
             np[NSMAX],     ///<??? np[isp] is probably the nb. of particles of species isp.
             nbin[NSMAX],   ///<Nb. of bins for energy distribution diagnostic of the species at left wall/in the system.
             inject[NSMAX], ///<inject[isp] is 1 if there are any particles of species isp injected from the left or right electrode, and 0 otherwise.
             nbin_mid[NSMAX],
             sp_k[NSMAX],   ///<Named 'k' in input files, usually has value of 1 for all species.
             it[NSMAX],     ///<it[species] is the current time-step of said species.
             maxnp[NSMAX],  ///<maxnp[species_id] is the max nb. of particles of said species.
             k_count[NSMAX], ndiag,
             gas,           ///<Index of the gas to use.
             psource,       ///<If 1, starts a uniform ionization source which compensates every ion leaving with an ion-electron pair. ??? - injects 2 particles for every 1 lost/only works for lost ions?
             nstrt,         ///<Initially, the nb of time-steps after which the vol_source and p_source particles sources activate, then is recycled for the field-solver.
             vel_dist_accum,
             vxloader[NSMAX][2], vyloader[NSMAX], vzloader[NSMAX], N_trapped[NSMAX],
             N_untrapped[NSMAX], E_trapped[NSMAX], E_untrapped[NSMAX], E_particles[NSMAX],

             rfa_left_grid_is_shorted,  ///<1 if the left rfa grid is held at a potential with a short-circuit, 0 otherwise.
             rfa_right_grid_is_shorted, ///<1 if the right rfa grid is held at a potential with a short-circuit, 0 otherwise.
             rfa_nb_steps,              ///<Nb of equal stepwise changes that the RFA voltage goes through between U_1_min and U_1_max.
             rfa_sampling_phase,
             rfa_hist_hi,
             edist_nb_bins;

long int seed; ///<Seed used for the frand() random number generator. Is set to process id.

double t,      ///<Elapsed simulation time, in seconds.
       rfa_init_time,           ///<Time elapsed between simulation start and rfa sampling start.
       rfa_stabilization_time,  ///<Time elapsed between external voltage change and start of current sampling.
       rfa_sample_time,         ///<Time used to sample the current.
       rfa_current_sample_time, ///<Time elapsed in the current rfa measurement.
       edist_init_time;

char src,      ///<Type of external circuit source. 'i' or 'I' for a current source, 'v' for a voltage source.
     **rate_title;

float **x,    ///<x[species_id][particle_id] is the x coordinate of a particle, where species_id < nsp and particle_id < maxnp[species_id]. Is scaled from [0,length] to [0,xnc].
      **vx,   ///<vx[species_id][particle_id] is the x-component of velocity of a particle, where species_id < nsp and particle_id < maxnp[species_id]. Scaled so x ~> x + vx, where x is also scaled.
      **vy,   ///<vy[species_id][particle_id] is the y-component of velocity of a particle, where species_id < nsp and particle_id < maxnp[species_id].
      **vz,   ///<vz[species_id][particle_id] is the z-component of velocity of a particle, where species_id < nsp and particle_id < maxnp[species_id].
      **sp_n, ///<sp_n[isp][i] is the nb of particles of species isp in cell i. Max indices sp_n[nsp][ng].
      *rho,   ///<Total charge density in each cell, in C/m^3. Max index rho[nc].
      *e,     ///<E field, possibly scaled to E/(eps*eps_0). Max index e[nc].
      *phi,   ///<Electrostatic potential, in volts. Max index phi[nc].
      *a,     ///<Precomputed acceleration of particle in cell, reused for each species. Max index a[nc].
      *x_grid,
      **fe,   ///<Histogram of particle energy distribution at LHS wall (scaled).
      **ftheta, **e_array, **th_array, **fe_mid, **fe_mid_show, //For diagnostics. th stands for angular distribution something.
      **sp_fe_ave, **sp_fe_show, **sp_fe,
      **e_mid_array,
      *e_ave,     ///<Electric field averaged by a few time-steps.
      *e_ave_show, *phi_ave, *phi_ave_show,
      **sp_n_ave, ///<Nb of particles in cell, averaged over a few time-steps.
      **sp_n_ave_show,
      **sp_n_0, **sp_n_k,
      **sp_n_mcc, ///<??? Density of particles created by Monte-Carlo collisions.
      *chi,       ///<Numerical (implicit) susceptibility of a cell. Max index chi[nc].
      **jdote, **jdote_show,
      **mccrate, **rate_show, **np_trapped, **np_untrapped, **kes_x_hist,
      **TE_trapped, **TE_untrapped, **TE_particle;

/* stuff for history diagnostics */
float *t_array,
      **np_hist, ///<History of the nb. of computer particles of each species.
      **ncp_hist_area_0,
      **ncp_hist_area_1,
      **ncp_hist_area_2,
      **jwall_hist, ///<History of the charge surface density (not current) on the LHS plate, sorted by contributing particle species.
      **phi_hist,
      *wall_sigma_hist, ///<History of the total charge surface density on the LHS plate.
      *ese_hist,
      **kes_hist, **kes_x_hist,  **kes_y_hist,  **kes_z_hist,
      *cur_hist,
      *com_cur_hist, *pow_hist, *com_pow_hist, **com_phi_hist, *f_array,
      *current_hist_smoothed,
      *cur_fft, *phi_fft, *pow_fft, *mphi_fft, *Local_t_array,
      **rfa_left_grid_sigma_hist, *rfa_left_grid_sigma_total_hist,
      **rfa_left_grid_jp_hist, *rfa_left_grid_jp_total_hist,
      *rfa_left_grid_phi_hist,
      **rfa_right_grid_sigma_hist, *rfa_right_grid_sigma_total_hist,
      **rfa_right_grid_jp_hist, *rfa_right_grid_jp_total_hist,
      *rfa_right_grid_phi_hist,
      **rfa_jwall_hist, ///<Per species current density history on the LHS plate, in A/m^2.
      *rfa_jwall_total_hist, ///<Total current density history on the LHS plate, in A/m^2.
      *rfa_U_1_hist,
      *rfa_intergrid_phi_max_hist, ///<Max potential reached at any time during RFA sampling, between RFA grids, in volts.
      *rfa_intergrid_phi_max_thist, ///<Max potential at any position between RFA grids, in volts.
      *rfa_avg_intergrid_phi_max_hist, ///<Time average of the max potential between RFA grids during RFA sampling, in volts.
      **edist_histogram_0,
      **edist_histogram_1,
      ***edist_histogram,
      ***edist_histogram_normalized,
      *edist_x_grid,
      *edist_histogram_energy_axis;

/* stuff for velocity moments diagnostics */
float **sp_ke_x, **sp_ke_y, **sp_ke_z,
      **sp_ke_x_show, **sp_ke_y_show, **sp_ke_z_show,
      **sp_ke_x_ave, **sp_ke_y_ave, **sp_ke_z_ave,
      **sp_ke_x_ave_show, **sp_ke_y_ave_show, **sp_ke_z_ave_show,
      **sp_ke_show, **sp_ke_ave_show,
      **sp_j_x, ///<Probably the current generated by species.
      **sp_j_y, **sp_j_z,
      **sp_j_x_show, **sp_j_y_show, **sp_j_z_show,
      **sp_j_x_ave, **sp_j_y_ave, **sp_j_z_ave,
      **sp_j_x_ave_show, **sp_j_y_ave_show, **sp_j_z_ave_show,
      **sp_u_x_show,      ///<X velocity of a species averaged over a single cell.
      **sp_u_y_show,      ///<Y velocity of a species averaged over a single cell.
      **sp_u_z_show,      ///<Z velocity of a species averaged over a single cell.
      **sp_u_x_ave_show,  ///<X velocity of a species averaged over a single cell and over a few time-steps.
      **sp_u_y_ave_show,  ///<Y velocity of a species averaged over a single cell and over a few time-steps.
      **sp_u_z_ave_show,  ///<Z velocity of a species averaged over a single cell and over a few time-steps.
      **Tx_ave, **Ty_ave, **Tz_ave,
      **Tx_ave_show, **Ty_ave_show, **Tz_ave_show, **T_ave_show;

/*stuff for velocity distribution diagnostics*/
float ***vx_dist, **vx_array,***vy_dist, **vy_array,***vz_dist, **vz_array;
float  vxu[NSMAX], ///<Upper x velocity for velocity distribution diagnostics, in m/s. (vx_upper in doc)
       vxl[NSMAX], ///<Lower x velocity for velocity distribution diagnostics, in m/s. (vx_lower in doc)
       vyu[NSMAX], ///<Upper y velocity for velocity distribution diagnostics, in m/s. (vy_upper in doc)
       vyl[NSMAX], ///<Lower y velocity for velocity distribution diagnostics, in m/s. (vy_lower in doc)
       vzu[NSMAX], ///<Upper z velocity for velocity distribution diagnostics, in m/s. (vz_upper in doc)
       vzl[NSMAX]; ///<Lower z velocity for velocity distribution diagnostics, in m/s. (vz_lower in doc)
int    nvxbin[NSMAX], ///<Nb. of bins used for x velocity distribution diagnostics (if 0 diagnostics is turned off). (nxbin in doc)
       nvybin[NSMAX], ///<Nb. of bins used for y velocity distribution diagnostics (if 0 diagnostics is turned off). (nybin in doc)
       nvzbin[NSMAX]; ///<Nb. of bins used for z velocity distribution diagnostics (if 0 diagnostics is turned off). (nzbin in doc)

/*stuff for the volume source */
float endpts[2],         ///<The uniform volume ionization source is only active in the range from endpts[0] to endpts[1].
      vol_source,        ///<Initially, the intensity of the uniform ionization source, in 1/(m^3 s), but gets converted to the nb of ionization events per dt.
      ionization_energy; ///<??? Energy, in eV, of the ion-electron pairs produced by the volume ionization source.

/*stuff for injection*/
float eold[4],      ///<Stores the old electric field. Index 0 is at the LHS, 1 at the left rfa grid, 2 at the right rfa grid, and 3 at the RHS.
      W[NSMAX],     ///<Species-specific constant coefficient, used by injection_push.
      sin4W[NSMAX], sin22W[NSMAX], cos22W[NSMAX], cos_psi, sin_psi;

float frand(void);                   ///<Returns a random floating point number in the range [0,1].
float tstrt;                         ///<Volume source of ionization occurs only when psource=true && t>tstrt.
//float bit_rever_maxwellian(void), maxwellian(void), maxwellian_flux(void);
float maxwellian(const int type); ///<Mostly returns a random number with a normal distribution of variance 1 and mean 0.
// maxwellian_flux(int);
float distribution(int,int,int),
      distribution_flux(const int k, const int isp, int /*type*/); ///<Returns the scaled thermal distribution of vx of species isp at the left (k=0) or right (k=1) side.
                                                                   /**<Distribution is proportional to v*exp(-v^2), because the flux of particles
                                                                       is proportional to their distribution -and- speed.*/
void maxwellv(float *, float *, float *, float);
double revers_base(int,int), base2(void);
void history(void);                  ///<Initializes the history arrays.
void gather(const int isp);          ///<Assigns charge densities of species isp to the grid.
void adjust(const int isp);          ///<Removes particles of species isp that cross boundaries.
                                     /**<\warning Assumes index isp will be looped starting at 0 each tick.*/
int start(void);                     ///<Allocates arrays, initializes stuff, and reads the input file.*/
void fields(void);                   ///<Initializes the fields arrays.
void setrho(void);                   ///<Sets the initial charge density.
void imp_move(const int isp,         ///<The species that is moved.
              const int EnergyFlag   ///<Boolean on wether or not to track diagnostics.
              );                     ///<Implicit particle mover, advances positions and velocities, considering only the EM field, and no collisions. Only supported if mag. field b=0.
void exp_move(const int isp,         ///<The species that is moved.
              const int EnergyFlag   ///<Boolean on wether or not to track diagnostics.
              );                     ///<Explicit particle mover, advances positions and velocities, considering only the EM field, and no collisions.
void (*moveptr)(const int isp,       ///<The species that is moved.
                const int EnergyFlag ///<Boolean on wether or not to track diagnostics.
                );                   ///<Advances positions and velocities of species isp using the imp_move or exp_move functions.
void (*mccptr)(int isp);             ///<Monte-Carlo collisions for species isp.
void mccdiag_init(void);
void heliummcc(int isp), argonmcc(int isp), neonmcc(int isp), oxygenmcc(int isp), mcc(int isp);

#endif //__pdp1_h
