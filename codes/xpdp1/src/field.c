/** \file field.c
* \brief Circuit and field solving functions.*/

#include "pdp1.h"
#include <assert.h>

#define BC1    1
#define BC2    2
#define BC3    3
#define BC4    4

float source(void); ///<Returns the voltage or current currently produced by the circuit source.
float bc1(void);    ///<Stands for boundary-condition 1.
float bc2(void), bc3(void), bc4(void), (*bcptr)(void);
void  circuit1(void), circuit2(void), circuit3(void), circuit4(void), (*circuitptr)(void);
float *gam,   ///<Points to array of size ng.
      *tri_a, ///<A matrix diagonal for the Poisson solver, of size ng.
      *tri_b, ///<A matrix diagonal for the Poisson solver, of size ng.
      *tri_c, ///<A matrix diagonal for the Poisson solver, of size ng.
      se, ///<Is set to dx*dx/epsilon, which is dx^2/(eps*eps_0) if we use the physical meaning of eps.
      jp; ///<Current surface density flowing from the plasma to the LHS plate.
int   bc_flag; ///<Indicates which external circuit solver is used.
float cs;
void field_init(void);

/*void AdvanceLRC(float* const q0, float* const q0d, float* const q0dd,
                const float L0, const float R0, const float C0,
                const float V0, const float Ip0,
                const float dt);*/
void SolveTridiagonalDestructive(float *x,             ///<Initially contains input vector v, and returns solution x. Indexed with [0, ..., N-1].
                                 const float *const a, ///<Diagonal below the main diagonal, indexed with [1, ..., N-1].
                                 const float *const b, ///<Main diagonal, indexed with [0, ..., N-1].
                                 float *const c,       ///<Diagonal above the main diagonal, indexed with [0, ..., N-2].
                                 const size_t N        ///<Number of equations.
                                 );                    ///<Solves Ax = v, where A is a tridiagonal matrix consisting of vectors a, b and c.
                                                       /**<\warning Contents of input vector c will be modified.*/
void AdvanceRFASampling(const float dt,
                        const double stabilization_time,
                        const double sample_time,
                        const int nb_steps,
                        const float U_min,
                        const float U_max,
                        float *const U,
                        double *const current_sample_time,
                        int *const sampling_phase);

/*************************************************/

void fields(void)
{
  static int init_flag=1;
  register int j, isp;
  float s, bet;

  if(init_flag) {
    field_init();
    init_flag = 0;
  }

  /* save old field on the end for injection */
  eold[0]=e[0];
  eold[1]=e[(int)rfa_left_grid_pos];
  eold[2]=e[(int)rfa_right_grid_pos];
  eold[3]=e[nc];

  /********************************************************/
  /* calculate total rho(x).  If implicit, also calculate */
  /* the numerical (implicit) susceptibility chi.  If     */
  /* explicit, sp_chi_scale=0 for all species => chi=0.   */

  for (j=0; j<=nc; j++) {
    rho[j] = rhoback;
    chi[j] = 0.0;
  }

  for (isp=0; isp<nsp; isp++){
    for (j=0; j<=nc; j++) {
      chi[j] += sp_n[isp][j]*sp_chi_scale[isp];
      rho[j] += sp_n[isp][j]*q[isp]/dx;
    }
  }

  /**********************************************************/
  /* recalculate the coeffs. for the tridiagnol field solve */

  if(!USE_RFA){ ///Version without RFA.
    for (j=1; j<nc; j++) {
      tri_a[j] = 1.0 + 0.5*(chi[j] + chi[j-1]);
      tri_c[j] = 1.0 + 0.5*(chi[j] + chi[j+1]);
      tri_b[j] = -(tri_a[j] + tri_c[j]);
    }
    if(bc_flag == BC1 || bc_flag == BC2) {
      tri_b[0] = -tri_a[1];
      tri_c[0] =  tri_a[1];
    }else if(bc_flag == BC4) {
      tri_b[0] = 2.25*extl/dt/dt + 1.5*extr/dt + 1/extc;
      tri_b[0] = -(tri_a[1] + dx/tri_b[0]/area/epsilon);
      tri_c[0] =  tri_a[1];
    }

    /*******************************************/
    /* solve a tridiagnal matrix to get phi[i] */

    jp = backj;

    for (isp=0; isp<nsp; isp++){
      jp += jwall[isp];
      //printf("%d \t %g \n",isp,jwall[isp]);
    }
    //printf("total j %g \n",jp);

    cs = source();

    if (nstrt) phi[0] = cs;
    bet = tri_b[nstrt];
    phi[nstrt] = (*bcptr)()/bet;

    for (j=nstrt+1; j<nc; j++) {
      gam[j] = tri_c[j-1]/bet;
      bet = tri_b[j] - tri_a[j]*gam[j];
      phi[j] = -(se*rho[j] + tri_a[j]*phi[j-1])/bet;
    }

    phi[nc] = 0.;
    for(j=nc-2; j>= nstrt; j--)
      phi[j] -= gam[j+1]*phi[j+1];
    /*******************************/
    /* calculate E(x) from phi(x)  */

    s = 0.5/dx;
    for (j=1; j<nc; j++)
      e[j] = (phi[j-1] - phi[j+1])*s;

    e[0]  = (1.0+0.5*(chi[0]+chi[1]))*(phi[0] -phi[1])/dx  -rho[0]*0.5*dx/epsilon;
    e[nc] = (1.0+0.5*(chi[nc-1]+chi[nc]))*(phi[nc-1] -phi[nc])/dx +rho[nc]*0.5*dx/epsilon;

    /****************************************/
    /* calculate the external circuit stuff */
    (*circuitptr)();
  }else{ ///Version with RFA.
    //Update the current density flowing to the LHS plate:
    jp = backj;
    for (isp=0; isp<nsp; ++isp)
      jp += jwall[isp];
    exti = area*jp;

    //Update plasma current density on rfa grids:
    rfa_left_grid_jp_total  = rfa_left_grid_opacity*backj; //TODO: Probably not okay - backj would not be homogenous if rfa grid drains some of it.
    rfa_right_grid_jp_total = rfa_right_grid_opacity*backj;
    for(isp = 0; isp < nsp; ++isp){
      rfa_left_grid_jp_total  += rfa_left_grid_jp[isp];
      rfa_right_grid_jp_total += rfa_right_grid_jp[isp];
    }

    //Update rfa grid charge densities:
    rfa_left_grid_oldsigma  = rfa_left_grid_sigma;
    rfa_right_grid_oldsigma = rfa_right_grid_sigma;
    if(!rfa_left_grid_is_shorted)
      rfa_left_grid_sigma  += rfa_left_grid_jp_total*dt;
    if(!rfa_right_grid_is_shorted)
      rfa_right_grid_sigma += rfa_right_grid_jp_total*dt;


    AdvanceRFASampling(dt, rfa_stabilization_time, rfa_sample_time, rfa_nb_steps, U_lgrid_min, U_lgrid_max,
                       &U_1, &rfa_current_sample_time, &rfa_sampling_phase);
    //U_2 = source(); //Uncomment if you wish U_2 to equal the source() voltage in the simulation.

    //Solve system Ax = v, where A is a tridiagonal matrix and x is the vector of potentials x[i] = phi[i].
    //tri_a is the subdiagonal, tri_b the diagonal, and tri_c the superdiagonal of the tridiagonal matrix A.
    //v corresponds to gam.

    //Set-up the tridiagonal matrix A:
    tri_a[0] =  0;
    tri_b[0] =  1;
    tri_c[0] =  0;

    for(j=1; j <= nc-1; ++j){
      tri_a[j] = -1;
      tri_b[j] =  2;
      tri_c[j] = -1;
    }

    if(rfa_left_grid_is_shorted){
      //thin version
      /*
      tri_a[(int)rfa_left_grid_pos] = 0;
      tri_b[(int)rfa_left_grid_pos] = 1;
      tri_c[(int)rfa_left_grid_pos] = 0;
      */
      for(j = -2; j <= 0; ++j){
        tri_a[(int)rfa_left_grid_pos + j] = 0;
        tri_b[(int)rfa_left_grid_pos + j] = 1;
        tri_c[(int)rfa_left_grid_pos + j] = 0;
      }
    }

    if(rfa_right_grid_is_shorted){
      //thin version
      /*
      tri_a[(int)rfa_right_grid_pos] = 0;
      tri_b[(int)rfa_right_grid_pos] = 1;
      tri_c[(int)rfa_right_grid_pos] = 0;
      */
      for(j = 0; j <= 2; ++j){
        tri_a[(int)rfa_right_grid_pos + j] = 0;
        tri_b[(int)rfa_right_grid_pos + j] = 1;
        tri_c[(int)rfa_right_grid_pos + j] = 0;
      }
    }

    //thin version
    /*
    tri_a[nc] = 0;
    tri_b[nc] = 1;
    tri_c[nc] = 0;
    */
    for(j = -1; j <= 0; ++j){
      tri_a[nc + j] = 0;
      tri_b[nc + j] = 1;
      tri_c[nc + j] = 0;
    }

    //Set-up the vector v/gam:
    gam[0] = U_lwall; //LHS plate is shorted to potential U_0.

    for(j = 1; j <= nc-1; ++j)
      gam[j] = se*rho[j];

    if(rfa_left_grid_is_shorted){
      //gam[(int)rfa_left_grid_pos]  = U_1; //thin version
      for(j = -2; j <= 0; ++j){
        gam[(int)rfa_left_grid_pos + j]  = U_1;
      }
    }else{
      gam[(int)rfa_left_grid_pos] += se*rfa_left_grid_sigma/dx; //TODO: make this grid 2dx thick. It is currently thin.
    }

    if(rfa_right_grid_is_shorted){
      //gam[(int)rfa_right_grid_pos]  = U_2; //thin version
      for(j = -2; j <= 0; ++j){
        gam[(int)rfa_right_grid_pos + j]  = U_2;
      }
    }else{
      //TODO: This is a terrifying hack to make the grid 2dx thick, without having to solve a pentadiagonal matrix.
      //      It is strongly advised to rewrite the potential calculation properly - you should end up with a pentadiagonal
      //      matrix - but mostly tridiagonal, except at floating grid indices.
      //gam[(int)rfa_right_grid_pos] += se*rfa_right_grid_sigma/dx; //old version
      static float sedrho = 0;
      sedrho += 0.5*(phi[(int)rfa_right_grid_pos+2] - phi[(int)rfa_right_grid_pos]);
      gam[(int)rfa_right_grid_pos] += se*rfa_right_grid_sigma/dx/2 + gam[(int)rfa_right_grid_pos+1]/2 + sedrho;
      gam[(int)rfa_right_grid_pos+2] += se*rfa_right_grid_sigma/dx/2 + gam[(int)rfa_right_grid_pos+1]/2 - sedrho;
      gam[(int)rfa_right_grid_pos+1] = 0;
    }

    gam[nc] = 0;
    ///More thick version stuff:
    gam[nc-1] = 0;


    //Solve the tridiagonal system:
    SolveTridiagonalDestructive(gam, tri_a, tri_b, tri_c, ng);
    for(j = 0; j <= nc; ++j)
      phi[j] = gam[j]; //TODO: Consider using phi instead of gam from the start.

    //Calculate E(x) from phi(x):
    s = 0.5/dx;
    for (j=1; j<nc; j++)
      e[j] = (phi[j-1] - phi[j+1])*s; // = dPhi/2dx

    e[0]  = (1.0+0.5*(chi[0]+chi[1]))*(phi[0] -phi[1])/dx  -rho[0]*0.5*dx/epsilon;
    //e[nc] = (1.0+0.5*(chi[nc-1]+chi[nc]))*(phi[nc-1] -phi[nc])/dx +rho[nc]*0.5*dx/epsilon;
    //e[nc] = (phi[nc-1] - phi[nc])/dx + rho[nc]*0.5*dx/epsilon;
    e[nc] = (phi[nc-1] - phi[nc])/dx;

    //Update shorted rfa grid surface charge densities (non-shorted ones are already updated):
    if(rfa_left_grid_is_shorted){
      const int lgi = rfa_left_grid_pos;
      rfa_left_grid_sigma  = (dx/se)*(- phi[lgi-1] + 2*phi[lgi] - phi[lgi+1] - se*rho[lgi]);
    }
    if(rfa_right_grid_is_shorted){
      const int rgi = rfa_right_grid_pos;
      rfa_right_grid_sigma = (dx/se)*(- phi[rgi-1] + 2*phi[rgi] - phi[rgi+1] - se*rho[rgi]);
    }
  }
}

/***************************************************************/

float source(void)
{
  if (dcramped) {
    if(t < risetime) {
      if(ramp) return (ramp*t);
      else     return (.5*dcbias*(1- cos(t*w0)));
    }else{
               return (dcbias);
    }
  }
  return dcbias + ramp*t + acbias*sin(t*w0 + theta0);
}

/***************************************************************/
/* Initalizing the arrays and parameters for the field solve   */

void field_init(void)
{
  /* Setting up the arrays for the poisson solve */
  gam  = (float *)malloc(ng*sizeof(float));
  tri_a= (float *)malloc(ng*sizeof(float));
  tri_b= (float *)malloc(ng*sizeof(float));
  tri_c= (float *)malloc(ng*sizeof(float));

  /******************************************/
  /* Deciding which circuit solver to use.. */
  if(extc < 1e-30) {   /* when C -> 0.0, OPEN CIRCUIT */
    if(fabs(dcbias)+fabs(ramp)+fabs(acbias) > 0)
      puts("START: Active external source is ignored since C < 1E-30\n");
    bc_flag = BC1;
    bcptr = bc1;
    circuitptr = circuit1;
    nstrt = 0;
  }else if(src== 'I' || src== 'i') {  /* When current source is applied */
    bc_flag = BC2;
    bcptr = bc2;
    circuitptr = circuit2;
    nstrt = 0;
  }else if(extl < 1e-30 && extr < 1e-30 && extc >= 1e5*epsilon*area/length) {
    /* When L=R=0.0 and C -> infinity, SHORT CIRCUIT */
    bc_flag = BC3;
    bcptr = bc3;
    circuitptr = circuit3;
    nstrt = 1;
  }else {             /* The general case with external voltage source */
    bc_flag = BC4;
    bcptr = bc4;
    circuitptr = circuit4;
    nstrt = 0;
  }
  se = dx*dx/epsilon;   /* setting se = dx*dx/epsilon */
}

/***************************************************************/
/* When C = 0: OPEN CIRCUIT */

float bc1()
{
  return -(sigma + dt*jp + 0.5*rho[0]*dx)*dx/epsilon;
}

void circuit1()
{
  oldsigma= sigma;
  sigma  += jp*dt;
}

/***************************************************************/
/* When CURRENT SOURCE is applied */

float bc2()
{
  return -(sigma + dt*(cs/area + jp) + 0.5*rho[0]*dx)*dx/epsilon;
}

void circuit2()
{
  exti    = cs;
  oldsigma= sigma;
  sigma  += dt*(jp + exti/area);
}

/***************************************************************/
/* When R=L=0, C -> infinity: SHORT CIRCUIT */

float bc3()
{
  return -rho[1]*dx*dx/epsilon - tri_a[1]*cs;
}

void circuit3()
{
  oldsigma = sigma;
  sigma = epsilon*e[0];
  if (t==0)
    oldsigma=sigma;
  exti = area*((sigma - oldsigma)/dt- jp);
}

/***************************************************************/
/* The General case */

float bc4()
{
  static float a0, a1, a2, a3, a4;
  float k;

  if(!a0) {
    a0 = 2.25*extl/dt/dt + 1.5*extr/dt + 1/extc;
    a1 = -6*extl/dt/dt - 2*extr/dt;
    a2 = 5.5*extl/dt/dt + .5*extr/dt;
    a3 = -2*extl/dt/dt;
    a4 = .25*extl/dt/dt;
  }
  k = (a1*extq + a2*extq_1 + a3*extq_2 + a4*extq_3)/a0;
  return -(0.5*rho[0]*dx + sigma + dt*jp + (cs/a0 - k - extq)/area)*dx/epsilon;
}

void circuit4() {
  static float a0, a1, a2, a3, a4;
  float k;

  if(!a0) {
    a0 = 2.25*extl/dt/dt + 1.5*extr/dt + 1/extc;
    a1 = -6*extl/dt/dt - 2*extr/dt;
    a2 = 5.5*extl/dt/dt + .5*extr/dt;
    a3 = -2*extl/dt/dt;
    a4 = .25*extl/dt/dt;
  }
  k = (a1*extq + a2*extq_1 + a3*extq_2 + a4*extq_3)/a0;
  extq_3 = extq_2;
  extq_2 = extq_1;
  extq_1 = extq;
  extq = (cs - phi[0])/a0 - k;

  exti = (extq - extq_1)/dt;
  oldsigma= sigma;
  sigma  += (jp + exti/area)*dt;
}

/***************************************************************/

/*void AdvanceLRC(float* const q0, float* const q0d, float* const q0dd,
                const float L0, const float R0, const float C0,
                const float V0, const float Ip0,
                const float dt)
{
  if(!(C0 > 0.f)){
    puts("AdvanceLRC: C0 must be > 0");
    exit(1);
  }

  static const int q_history_nb = 4;

  const float phi_left = V0 - q0[0]/C0 - q0d[0]*R0 - q0dd[0]*L0;

  if(L0 > 0){
    const float dI = q0[0]/C0 + q0d[0]*R0 + q0dd[0]*L0 + phi_left - ;
  }
}*/

void SolveTridiagonalDestructive(float *x, const float *a, const float *b, float *const c, const size_t N)
{
  c[0] = c[0] / b[0];
  x[0] = x[0] / b[0];

  size_t i;

  for (i = 1; i < N; ++i) {
    const float m = 1.0 / (b[i] - a[i] * c[i - 1]);
    c[i] = c[i] * m;
    x[i] = (x[i] - a[i] * x[i - 1]) * m;
  }

  for (i = N - 1; i-- > 0;) //Loop from N - 2 to 0 inclusive, avoiding unsigned wrap-around.
    x[i] = x[i] - c[i] * x[i + 1];
}

void AdvanceRFASampling(const float dt,
                        const double stabilization_time,
                        const double sample_time,
                        const int nb_steps,
                        const float U_min,
                        const float U_max,
                        float *const U,
                        double *const current_sample_time,
                        int *const sampling_phase)
{
  static int step_index = 0; // Steps [0,nb_steps] are upwards steps, and [nb_steps+1, 2*nb_steps+1] are downwards steps.

  assert(dt > 0 && stabilization_time > dt && sample_time > dt && nb_steps >= 1);
  assert(U != NULL && current_sample_time != NULL && sampling_phase != NULL);

  *current_sample_time += dt;
  if(*current_sample_time > stabilization_time+sample_time){
    *current_sample_time -= stabilization_time+sample_time;
    step_index = (step_index+1) % (2*nb_steps+2);
  }

  if(*current_sample_time > stabilization_time)
    *sampling_phase = 1; //Accumulate current.
  else if(*sampling_phase == 1)
    *sampling_phase = 2; //Move to next sample.
  else
    *sampling_phase = 0; //Wait.

  const float U_step_size = (U_max-U_min) / nb_steps;
  if(step_index <= nb_steps)
    *U = U_min + step_index*U_step_size;
  else // step_index is in range [nb_steps+1, 2*nb_steps+1]
    *U = U_max - (step_index-nb_steps-1)*U_step_size;
}
