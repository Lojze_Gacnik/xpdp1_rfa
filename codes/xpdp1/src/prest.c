#include "pdp1.h"

void realft(float data[], const int, const int);
void velocity(void), freqanalysis(void);
float bit_rever_maxwell(),
      maxwell(void); //Returns a random number with a normal distribution of variance 1 and mean 0.

int round_to_nearest(const float x);

int round_to_nearest(const float x){
  if(x >= 0.f)
    return x + 0.5f;
  else
    return x - 0.5f;
}

/**********************************************************************/

double base2()
{
  static int counter2=0;
  return(revers_base(counter2++, 2));
}

/**********************************************************************/

double revers_base(int num, int n)
{
  double power, rev;
  int inum, iquot, irem;

  rev = 0.;
  inum = num;
  power = 1.;

  do {
    iquot = inum/n;
    irem = inum - n*iquot;
    power /= n;
    rev += irem*power;
    inum = iquot;
  } while (inum > 0);

  return (rev);
}

/************************************************************************/
/* irand(iseed) returns values 1 through 2**31-2.                       */
/* From "Random number generators: good ones are hard to find", S. Park */
/* and K. Miller, Communications of ACM, October 1988, pp 1192-1201.    */
/* This is from page 1195, and is to work on any system for which       */
/* maxint is 2**31-1 or larger. Due earlier to Schrage, as cited by P&M.*/
/*                                                                      */
/* Note: OK values for iseed are 1 through 2**31-2. Give it 0 or 2*31-1 */
/* and it will return the same values thereafter!                       */
/*                                                                      */
/* C version 6/91, Bruce Langdon.                                       */
/*                                                                      */
/* Algorithm replaces seed by mod(a*seed,m). First represent            */
/* seed = q*hi + lo.  Then                                              */
/* a*seed = a*q*hi + lo = (m - r)*hi + a*lo = (a*lo - r*hi) + m*hi,     */
/* and new seed = a*lo - r*hi unless negative; if so, then add m.       */

float frand(void)
{
  long af = 16807, mf = 2147483647, qf = 127773, r = 2836;
  long hi, lo;
  float fnumb;

  hi = seed/qf;
  lo = seed - qf*hi;
  seed = af*lo - r*hi;
  /* "seed" will always be a legal integer of 32 bits (including sign). */
  if(seed <= 0) seed = seed + mf;
  fnumb = seed/2147483646.0;

  return(fnumb);
}

/**********************************************************************/

float bit_rever_maxwell()
{
  static int iset=0;
  static int count=0;
  static float gset;
  float fac,r,v1,v2;

  if(iset==0){
    do{
      v1=2.0*revers_base(count++,2)-1.0;
      v2=2.0*revers_base(count,5)-1.0;
      r = v1*v1+v2*v2;
    } while (r>=1.0);
    fac=sqrt(-2*log(r)/r);
    gset=v1*fac;
    iset = 1;
    return v2*fac;
  }
  else{
    iset=0;
    return gset;
  }
}

/**********************************************************************/

float maxwell()
{
  //Source: Numerical Recipes, 2008, page 365
  static int iset=0;
  static float gset;
  float fac,r2,v1,v2;

  if(iset==0){
    do{
      v1 = 2.0*frand() - 1.0;
      v2 = 2.0*frand() - 1.0;
      r2 = v1*v1+v2*v2;
    } while (r2 >= 1.0);
    fac=sqrt(-2*log(r2)/r2);
    gset=v1*fac;
    iset = 1;
    return v2*fac;
  }
  else{
    iset=0;
    return gset;
  }
}

/**********************************************************************/

float maxwellian(const int type)
{
  switch (type){
  case RANDOM:
    return maxwell();
    break;
  case BIT_REVERSE:
    return bit_rever_maxwell();
    break;
  default:
    puts("LOAD: Bad value for loader flag");
    exit(-1);
    break;
  }
}

/**************************************************************/

#define  NVTC    3.3 // number of thermal velocity for upper cutoff

float distribution(int k, int isp, int type)
{
  static int init=1;
  static int cutoff=1;
  static int init_sp[NSMAX][2];
  static int nvel[NSMAX][2];
  static float *v[NSMAX][2];
  float erfvl, diff_erf, vlower, vupper, flower, fupper, f, fmid;
  float dv, dvi;
  int i, n, ksign;
  float vnew;

  if(init){
    for(i=0; i<NSMAX; i++)
      for(ksign=0; ksign<2; ksign++)
        init_sp[i][ksign] = 1;
    init = 0;
  }

  if (init_sp[isp][k]) {
    init_sp[isp][k] = 0;
    if (vc[isp][k]!=0){
      erfvl = erf(vc[isp][k]);
      diff_erf = erf((float)NVTC)-erfvl;
      nvel[isp][k] = 1.0/(1-diff_erf/(1-erfvl));
      v[isp][k] = (float *) malloc(nvel[isp][k]*sizeof(float));

      v[isp][k][0] = vc[isp][k];
      v[isp][k][nvel[isp][k]-1] = NVTC;
      dvi = (NVTC-vc[isp][k])/((float)nvel[isp][k]);
      dv = dvi;
      fmid = 0;
      vlower = vc[isp][k];
      f = 0;
      flower = erf(vlower)-f*(diff_erf)-erfvl;
      vupper = vlower + dv;
      for (n=1; n<(nvel[isp][k]-1); n++){
        f = (float)n/(float)(nvel[isp][k]-1);
        flower = erf(vlower) - f*diff_erf - erfvl;
        fupper = erf(vupper) - f*diff_erf - erfvl;
        while ((flower*fupper)>0.0){
          dv*=2;
          vupper = vlower + dv;
          fupper = erf(vupper) - f*diff_erf - erfvl;
        }
        while (dv>1e-8){
          dv /=2;
          fmid = erf(vlower+dv) - f*diff_erf - erfvl;
          if (fmid<=0) vlower += dv;
          else         vupper -= dv;
        }
        v[isp][k][n] = v0[isp][k]+vt[isp][k]*vlower;
      }
    }else{
      cutoff = 0.0;
    }
  }

  if (vt[isp][k]==0){
    vnew = v0[isp][k];
  }else if (cutoff){
    switch (type){
      case RANDOM:
        vnew = v[isp][k][(int)(nvel[isp][k]*frand())];
        break;
      case BIT_REVERSE:
        vnew = v[isp][k][(int)(nvel[isp][k]*base2())];
        break;
      default:
        puts("LOAD: Bad value for loader flag");
        exit(-1);
        break;
    }
  }else{
    vnew = v0[isp][k]+vt[isp][k]*fabs(maxwellian(type));
  }

  return vnew;
}

/**************************************************************/
/*float maxwellian_flux(void) {
  return sqrt(-log(frand()));
}*/

float distribution_flux(const int k, const int isp, int type)
{
  static int init=1;
  static int cutoff=1;
  static int init_sp[NSMAX][2];
  static float vc2[NSMAX][2];
  float v;
  int i,ksign;

  if(init){
    for(i=0; i<NSMAX; i++)
      for(ksign=0; ksign<2; ksign++)
        init_sp[i][ksign] = 1;
    init = 0;
  }
  if (init_sp[isp][k]){
    if (vc[isp][k] == 0) cutoff = 0;
    else                 vc2[isp][k] = vc[isp][k]*vc[isp][k];
    init_sp[isp][k] = 0;
  }

  if (!cutoff)  v = v0[isp][k] + vt[isp][k]*sqrt(-2*log(frand()));
  else          v = v0[isp][k] + vt[isp][k]*sqrt(vc2[isp][k]-log(frand()));

  return v;
}

/************************************************************************/
/* time history accumulator; calculates and stores all history values,  */
/* and performs combing on history values when low on memory            */
void history(void)
{
  register int i, j, isp, k;
  static float jtemp[NSMAX];
  static int count=1, nrftcount=0, nrfcount=0;

  /******   MCC rates  ******/
  if(it[0]) {
    for(i=0; i<ndiag; i++)
      for (j=0; j<ng; j++)
        rate_show[i][j] = (rate_show[i][j]*(it[0]-1) +mccrate[i][j])/((float)it[0]);
  }

  /*****************************************/
  ///  Fixing the diagnostic arrays

  if (vel_dist_accum) velocity();

  if(nsp && it[0]) {
    for (isp=0; isp<nsp; isp++) {
      jtemp[isp] += dt*jwall[isp]; //This is scaled by dt because, despite its name, it represents surface charge density, not current density.

      for (i=0; i<nbin_mid[isp]; i++)
        fe_mid_show[isp][i] = fe_mid[isp][i]/(it[isp]*sqrt(e_mid_array[isp][i]+DBL_MIN));

      for (i=0; i< ng; i++) {
        if(sp_n_k[isp][i] <1e-10) {
          sp_u_x_show[isp][i] = sp_u_y_show[isp][i] = sp_u_z_show[isp][i] = 0.0;
          sp_ke_show[isp][i] = sp_ke_x_show[isp][i] = sp_ke_y_show[isp][i] = sp_ke_z_show[isp][i] = 0.0;
        }else {
          sp_u_x_show[isp][i] = sp_j_x[isp][i]/sp_n_k[isp][i];
          sp_u_y_show[isp][i] = sp_j_y[isp][i]/sp_n_k[isp][i];
          sp_u_z_show[isp][i] = sp_j_z[isp][i]/sp_n_k[isp][i];
          sp_ke_x_show[isp][i] = Escale[isp]*sp_ke_x[isp][i]/sp_n_k[isp][i];
          sp_ke_y_show[isp][i] = Escale[isp]*sp_ke_y[isp][i]/sp_n_k[isp][i];
          sp_ke_z_show[isp][i] = Escale[isp]*sp_ke_z[isp][i]/sp_n_k[isp][i];
          sp_ke_show[isp][i]=sp_ke_x_show[isp][i]+sp_ke_y_show[isp][i]+sp_ke_z_show[isp][i];
        }
        jdote_show[isp][i]= jdote[isp][i]*jdote_scale[isp]/it[isp];
        sp_j_x_show[isp][i] = sp_j_x[isp][i]*j_scale[isp];
        sp_j_y_show[isp][i] = sp_j_y[isp][i]*j_scale[isp];
        sp_j_z_show[isp][i] = sp_j_z[isp][i]*j_scale[isp];
      }
    }
  }

  /*****************************************/

  if(nfft) {
    if (thist_hi >= nfft) {
      freqanalysis();
      thist_hi=0;
    }
    cur_hist[thist_hi]= exti;
    pow_hist[thist_hi]= exti*phi[0];
    phi_hist[0][thist_hi] = phi[0];
    phi_hist[1][thist_hi] = phi[nc/2];
    Local_t_array[thist_hi] = t;
    thist_hi++;
  }

  /*****************************************/
  /// Calculating the time ave. moments

  if (n_ave){
    if(nrftcount == n_ave) {
      nrfcount++;

      for(isp=0; isp<nsp; isp++){
        for(i=0; i<ng; i++) {
          if (sp_n_ave[isp][i]>DEN_MIN){
            sp_u_x_ave_show[isp][i] = sp_j_x_ave[isp][i]/sp_n_ave[isp][i];
            sp_u_y_ave_show[isp][i] = sp_j_y_ave[isp][i]/sp_n_ave[isp][i];
            sp_u_z_ave_show[isp][i] = sp_j_z_ave[isp][i]/sp_n_ave[isp][i];

            sp_ke_x_ave_show[isp][i] = sp_ke_x_ave[isp][i]/sp_n_ave[isp][i];
            sp_ke_y_ave_show[isp][i] = sp_ke_y_ave[isp][i]/sp_n_ave[isp][i];
            sp_ke_z_ave_show[isp][i] = sp_ke_z_ave[isp][i]/sp_n_ave[isp][i];

            Tx_ave_show[isp][i] = sp_ke_x_ave_show[isp][i]-sqr(sp_u_x_ave_show[isp][i]);
            Ty_ave_show[isp][i] = sp_ke_y_ave_show[isp][i]-sqr(sp_u_y_ave_show[isp][i]);
            Tz_ave_show[isp][i] = sp_ke_z_ave_show[isp][i]-sqr(sp_u_z_ave_show[isp][i]);

            sp_ke_x_ave_show[isp][i] *= Escale[isp];
            sp_ke_y_ave_show[isp][i] *= Escale[isp];
            sp_ke_z_ave_show[isp][i] *= Escale[isp];

            Tx_ave_show[isp][i] *= Escale[isp];
            Ty_ave_show[isp][i] *= Escale[isp];
            Tz_ave_show[isp][i] *= Escale[isp];
          }else{
            sp_u_x_ave_show[isp][i] = 0.0;
            sp_u_y_ave_show[isp][i] = 0.0;
            sp_u_z_ave_show[isp][i] = 0.0;

            sp_ke_x_ave_show[isp][i] = 0.0;
            sp_ke_y_ave_show[isp][i] = 0.0;
            sp_ke_z_ave_show[isp][i] = 0.0;

            Tx_ave_show[isp][i] = 0.0;
            Ty_ave_show[isp][i] = 0.0;
            Tz_ave_show[isp][i] = 0.0;
          }

          T_ave_show[isp][i] = Tx_ave_show[isp][i]+Ty_ave_show[isp][i]+Tz_ave_show[isp][i];
          sp_ke_ave_show[isp][i] = sp_ke_x_ave_show[isp][i]+sp_ke_y_ave_show[isp][i]+sp_ke_z_ave_show[isp][i];

          sp_ke_x_ave[isp][i] = sp_ke_x[isp][i];
          sp_ke_y_ave[isp][i] = sp_ke_y[isp][i];
          sp_ke_z_ave[isp][i] = sp_ke_z[isp][i];

          sp_n_ave_show[isp][i] = sp_n_ave[isp][i]/n_ave; sp_n_ave[isp][i] = sp_n[isp][i];
          sp_j_x_ave_show[isp][i] = sp_j_x_ave[isp][i]/n_ave; sp_j_x_ave[isp][i] = sp_j_x[isp][i];
          sp_j_y_ave_show[isp][i] = sp_j_y_ave[isp][i]/n_ave; sp_j_y_ave[isp][i] = sp_j_y[isp][i];
          sp_j_z_ave_show[isp][i] = sp_j_z_ave[isp][i]/n_ave; sp_j_z_ave[isp][i] = sp_j_z[isp][i];
        }
        for (i=0; i<nbin_mid[isp]; i++){
          sp_fe_show[isp][i] = sp_fe_ave[isp][i]/(n_ave*sqrt(e_mid_array[isp][i]+DBL_MIN));
          sp_fe_ave[isp][i] = sp_fe[isp][i];
        }
      }
      for (i=0; i<ng; i++) {
        e_ave_show[i]= e_ave[i]/n_ave;      e_ave[i]  = e[i];
        phi_ave_show[i]= phi_ave[i]/n_ave;  phi_ave[i]= phi[i];
      }

      nrftcount = 1;
    }else {
      for(isp=0; isp<nsp; isp++){
        for(i=0; i<ng; i++) {
          sp_n_ave[isp][i] += sp_n[isp][i];
          sp_j_x_ave[isp][i] += sp_j_x[isp][i];
          sp_j_y_ave[isp][i] += sp_j_y[isp][i];
          sp_j_z_ave[isp][i] += sp_j_z[isp][i];
          sp_ke_x_ave[isp][i]+= sp_ke_x[isp][i];
          sp_ke_y_ave[isp][i]+= sp_ke_y[isp][i];
          sp_ke_z_ave[isp][i]+= sp_ke_z[isp][i];
        }
        for (i=0; i<nbin_mid[isp]; i++)
          sp_fe_ave[isp][i] += sp_fe[isp][i];
      }
      for (i=0; i<ng; i++) {
        e_ave[i]   += e[i];
        phi_ave[i] += phi[i];
      }
      nrftcount++;
    }
  }

  for(isp=0; isp<nsp; isp++)
    for (i=0; i<nbin_mid[isp]; i++)
      sp_fe[isp][i]=0.0;

  /********************************************/

  if(rfa_hist_hi < HISTMAX){
    if(rfa_sampling_phase == 1){ //Accumulate current.
      for(isp = 0; isp < nsp; ++isp){
        rfa_jwall_hist[isp][rfa_hist_hi] += jwall[isp]*dt/rfa_sample_time;
        rfa_jwall_total_hist[rfa_hist_hi] += jwall[isp]*dt/rfa_sample_time;
      }
      rfa_U_1_hist[rfa_hist_hi] = U_1;

      const int lgrid_index = rfa_left_grid_pos;
      const int rgrid_index = rfa_right_grid_pos;
      const float ticks_per_sample = rfa_sample_time/dt;

      for(i = lgrid_index; i <= rgrid_index; ++i){
        if(rfa_intergrid_phi_max_hist[rfa_hist_hi] < phi[i]) rfa_intergrid_phi_max_hist[rfa_hist_hi] = phi[i];
      }
      rfa_avg_intergrid_phi_max_hist[rfa_hist_hi] += rfa_intergrid_phi_max_hist[rfa_hist_hi]/ticks_per_sample;
    }else if(rfa_sampling_phase == 2){ //Sample end.
      ++rfa_hist_hi;
    }
  }

  if (--count) return;    /* only accum every interval steps */
  if (hist_hi >= HISTMAX) {  /* comb time histories */
    for (isp=0; isp<nsp; isp++) {
      for (i=1, k=4; i<HISTMAX/4; i++, k+=4) {
        np_hist[isp][i] = np_hist[isp][k];
        ncp_hist_area_0[isp][i] = ncp_hist_area_0[isp][k];
        ncp_hist_area_1[isp][i] = ncp_hist_area_1[isp][k];
        ncp_hist_area_2[isp][i] = ncp_hist_area_2[isp][k];
        np_trapped[isp][i] = np_trapped[isp][k];
        np_untrapped[isp][i] = np_untrapped[isp][k];
        TE_trapped[isp][i] = TE_trapped[isp][k];
        TE_untrapped[isp][i] = TE_untrapped[isp][k];
        TE_particle[isp][i] = TE_particle[isp][k];
        kes_hist[isp][i] = kes_hist[isp][k];
        kes_x_hist[isp][i] = kes_x_hist[isp][k];
        kes_y_hist[isp][i] = kes_y_hist[isp][k];
        kes_z_hist[isp][i] = kes_z_hist[isp][k];
        jwall_hist[isp][i] = jwall_hist[isp][k];

        rfa_left_grid_sigma_hist[isp][i]  = rfa_left_grid_sigma_hist[isp][k];
        rfa_left_grid_jp_hist[isp][i]     = rfa_left_grid_jp_hist[isp][k];
        rfa_right_grid_sigma_hist[isp][i] = rfa_right_grid_sigma_hist[isp][k];
        rfa_right_grid_jp_hist[isp][i]    = rfa_right_grid_jp_hist[isp][k];
      }
    }
    for (i=1, k=4; i<HISTMAX/4; i++, k+=4) {
      com_phi_hist[0][i] = com_phi_hist[0][k];
      com_phi_hist[1][i] = com_phi_hist[1][k];
      com_pow_hist[i] = com_pow_hist[k];
      com_cur_hist[i] = com_cur_hist[k];
      wall_sigma_hist[i] = wall_sigma_hist[k];
      ese_hist[i] = ese_hist[k];
      t_array[i] = t_array[k];

      rfa_left_grid_sigma_total_hist[i]  = rfa_left_grid_sigma_total_hist[k];
      rfa_right_grid_sigma_total_hist[i] = rfa_right_grid_sigma_total_hist[k];
      rfa_left_grid_jp_total_hist[i]  = rfa_left_grid_jp_total_hist[k];
      rfa_right_grid_jp_total_hist[i] = rfa_right_grid_jp_total_hist[k];
      rfa_left_grid_phi_hist[i]  = rfa_left_grid_phi_hist[k];
      rfa_right_grid_phi_hist[i] = rfa_right_grid_phi_hist[k];
      rfa_intergrid_phi_max_thist[i] = rfa_intergrid_phi_max_thist[k];

      ncp_hist_area_0[i] = ncp_hist_area_0[k];
      ncp_hist_area_1[i] = ncp_hist_area_1[k];
      ncp_hist_area_2[i] = ncp_hist_area_2[k];
    }
    hist_hi = i;
    interval *= 4;
  }

  if(1/*t >= edist_init_time*/){
    const float vel_unscaling_factor = (length/xnc/dt)*(length/xnc/dt); //Multiplying the square of velocity with this, gives the square of velocity in (m/s)^2
    const float bin_width = (edist_e_max-edist_e_min) / (edist_nb_bins-1);

    for (isp=0; isp<nsp; isp++) {    /* accumulate histories */
      int ii;
      for(ii = 0; ii < np[isp]; ++ii){
        const int is_in_area_0 = (edist_area_0_start <= x[isp][ii]) && (x[isp][ii] <= edist_area_0_end);
        const int is_in_area_1 = (edist_area_1_start <= x[isp][ii]) && (x[isp][ii] <= edist_area_1_end);

        if(1 /*is_in_area_0 || is_in_area_1*/){
          const float velocity_squared = vel_unscaling_factor * (vx[isp][ii]*vx[isp][ii] + vy[isp][ii]*vy[isp][ii] + vz[isp][ii]*vz[isp][ii]); //In (m/s)^2
          const float kinetic_energy = 0.5f*velocity_squared*m[isp];

          const float eVolts_in_joule = 6.241509e18;
          const float kinetic_energy_in_eV = kinetic_energy*eVolts_in_joule;
/*****************/
/**************/
          if(kinetic_energy_in_eV >= edist_e_min && kinetic_energy_in_eV <= edist_e_max){
            const int edist_index = round_to_nearest((kinetic_energy_in_eV-edist_e_min) / bin_width);

            if(edist_index >= 0 && edist_index < edist_nb_bins){
              edist_histogram_0[isp][edist_index] += is_in_area_0;
              edist_histogram_1[isp][edist_index] += is_in_area_1;
              const int x_index = x[isp][ii];
              if(x_index > 0 && x_index < nc)
                edist_histogram[isp][x_index][edist_index] += 1;
            }
          }
        }
      }
    }
  }

  static int ticks_till_normalization = 0;
  if(ticks_till_normalization <= 0){
    ticks_till_normalization = 128;
    for(isp = 0; isp < nsp; ++isp){
      int x_index;
      for(x_index = 0; x_index < nc; ++x_index){
        float nb_of_particles = 0;
        int ii;
        for(ii = 0; ii < edist_nb_bins; ++ii)
          nb_of_particles += edist_histogram[isp][x_index][ii];
        const float inv_nb_of_particles = (nb_of_particles > 0) ? 1/nb_of_particles : 1;
        for(ii = 0; ii < edist_nb_bins; ++ii)
          edist_histogram_normalized[isp][x_index][ii] = edist_histogram[isp][x_index][ii]*inv_nb_of_particles;
      }
    }
  }
  --ticks_till_normalization;

  for (isp=0; isp<nsp; isp++) {    /* accumulate histories */
    jwall_hist[isp][hist_hi]= jtemp[isp]; //SEGFAULT HERE when hist_hi==32768/4 (when 32768 was equal to HISTMAX)
    jtemp[isp]= 0.0;
    if(hist_hi)  jwall_hist[isp][hist_hi] +=jwall_hist[isp][hist_hi-1];

    rfa_left_grid_sigma_hist[isp][hist_hi] = rfa_left_grid_jp[isp]*dt;
    if(hist_hi > 0) rfa_left_grid_sigma_hist[isp][hist_hi] += rfa_left_grid_sigma_hist[isp][hist_hi-1];
    rfa_left_grid_jp_hist[isp][hist_hi] = rfa_left_grid_jp[isp];

    rfa_right_grid_sigma_hist[isp][hist_hi] = rfa_right_grid_jp[isp]*dt;
    if(hist_hi > 0) rfa_right_grid_sigma_hist[isp][hist_hi] += rfa_right_grid_sigma_hist[isp][hist_hi-1];
    rfa_right_grid_jp_hist[isp][hist_hi] = rfa_right_grid_jp[isp];


    np_hist[isp][hist_hi] = np[isp];
    for(j = 0; j < np[isp]; ++j){
      ncp_hist_area_0[isp][hist_hi] += (x[isp][j] <= rfa_left_grid_pos);
      ncp_hist_area_1[isp][hist_hi] += (x[isp][j] >= rfa_left_grid_pos && x[isp][j] <= rfa_right_grid_pos);
      ncp_hist_area_2[isp][hist_hi] += (x[isp][j] >= rfa_right_grid_pos);
    }
    np_trapped[isp][hist_hi] = N_trapped[isp];
    np_untrapped[isp][hist_hi] = N_untrapped[isp];
    TE_trapped[isp][hist_hi] = E_trapped[isp];
    TE_untrapped[isp][hist_hi] = E_untrapped[isp];
    TE_particle[isp][hist_hi] = E_particles[isp];
    kes_hist[isp][hist_hi] = 0;
    kes_x_hist[isp][hist_hi] = 0;
    kes_y_hist[isp][hist_hi] = 0;
    kes_z_hist[isp][hist_hi] = 0;

    for (j=0; j<ng; j++){
      kes_hist[isp][hist_hi]+=sp_ke_x[isp][j]+sp_ke_y[isp][j]+sp_ke_z[isp][j];
      kes_x_hist[isp][hist_hi]+=sp_ke_x[isp][j];
      kes_y_hist[isp][hist_hi]+=sp_ke_y[isp][j];
      kes_z_hist[isp][hist_hi]+=sp_ke_z[isp][j];
    }

    if(np[isp]) {
      kes_hist[isp][hist_hi] *= Escale[isp]/np[isp];  // note: 1/4 scaling in initwin.c
      kes_x_hist[isp][hist_hi] *= Escale[isp]/np[isp];
      kes_y_hist[isp][hist_hi] *= Escale[isp]/np[isp];
      kes_z_hist[isp][hist_hi] *= Escale[isp]/np[isp];
    }
  }
  t_array[hist_hi] = t;
  wall_sigma_hist[hist_hi] = sigma;
  com_cur_hist[hist_hi] = exti;

  for(i = 0; i < hist_hi; ++i){
    const int n_smooth = 600;
    float temp_sum = 0;
    int jj;
    for(jj = i-n_smooth; jj < i+n_smooth; ++jj)
      if(jj >= 0 && jj <= hist_hi)
        temp_sum += com_cur_hist[jj];
    current_hist_smoothed[i] = temp_sum/n_smooth;
  }

  com_phi_hist[0][hist_hi] = phi[0];
  com_phi_hist[1][hist_hi] = phi[nc/2];
  com_pow_hist[hist_hi] = exti*phi[0];

  rfa_left_grid_sigma_total_hist[hist_hi]  = rfa_left_grid_sigma;
  rfa_right_grid_sigma_total_hist[hist_hi] = rfa_right_grid_sigma;
  rfa_left_grid_jp_total_hist[hist_hi]  = rfa_left_grid_jp_total;
  rfa_right_grid_jp_total_hist[hist_hi] = rfa_right_grid_jp_total;

  const int lgrid_index = rfa_left_grid_pos;
  const int rgrid_index = rfa_right_grid_pos;
  rfa_left_grid_phi_hist[hist_hi] = phi[lgrid_index];
  rfa_right_grid_phi_hist[hist_hi] = phi[rgrid_index];

  rfa_intergrid_phi_max_thist[hist_hi] = phi[rgrid_index];
  for(i = lgrid_index; i < rgrid_index; ++i)
    if(rfa_intergrid_phi_max_thist[hist_hi] < phi[i])
      rfa_intergrid_phi_max_thist[hist_hi] = phi[i];

  ese_hist[hist_hi] = 0.5*(rho[0]*phi[0] + rho[nc]*phi[nc]);
  for (i=1; i<nc; i++) ese_hist[hist_hi] += rho[i]*phi[i];
  ese_hist[hist_hi] *= 0.5*area*dx;
  ese_hist[hist_hi] -= epsilon*e[nc]*area*phi[nc]; // what is this here for, phi[nc]==0
  ese_hist[hist_hi] += .5*sigma*area*phi[0];  // add in induced wall charge.

  ese_hist[hist_hi] = fabs(ese_hist[hist_hi]+DBL_MIN);
  for (isp=0; isp<nsp; isp++)
    kes_hist[isp][hist_hi] = kes_hist[isp][hist_hi]+DBL_MIN;

  hist_hi++;
  count = interval;
}

/***************************************************************/

void freqanalysis()
{
  register int i, j;
  static int init_freq_flag=1;
  static float *temp1, *temp2;

  if(init_freq_flag) {
    if(!(temp1= (float *)malloc((nfft/2)*sizeof(float)))
       || !(temp2= (float *)malloc((nfft/2)*sizeof(float))))
      puts("Null ptr in freqanalysis()");
    init_freq_flag=0;
  }

  for(i=0; i< nfft; i++) {
    cur_fft[i]= cur_hist[i];
    pow_fft[i]= pow_hist[i];
    phi_fft[i]= phi_hist[0][i];
    mphi_fft[i]= phi_hist[1][i];
  }

  realft(phi_fft-1, freq_hi, 1);
  realft(mphi_fft-1,freq_hi, 1);
  realft(cur_fft-1, freq_hi, 1);
  realft(pow_fft-1, freq_hi, 1);

  /******************************************************/
  /**** Computing mag and phase of the current signal ***/

  temp1[0]= fabs(cur_fft[0])/freq_hi/2;
  temp2[0]= (cur_fft[0] > 0.0) ? 0.0 : 180.0;
  for(i=1, j=2; i< freq_hi; i++, j+=2) {
    temp1[i]= sqrt(cur_fft[j]*cur_fft[j]+ cur_fft[j+1]*cur_fft[j+1])/freq_hi;
    if(fabs(cur_fft[j+1]) < 1e-30 && fabs(cur_fft[j]) < 1e-30)
      temp2[i]= 0.0;
    else
      temp2[i]= (180./M_PI)*atan2(cur_fft[j], cur_fft[j+1]);
  }
  for(i=0; i< freq_hi; i++) {
    cur_fft[i]= temp1[i];
    cur_fft[freq_hi+i]= temp2[i];
  }

  /******************************************************/
  /**** Computing mag and phase of the power signal *****/

  temp1[0]= fabs(pow_fft[0])/freq_hi/2;
  temp2[0]= (pow_fft[0] > 0.0) ? 0.0 : 180.0;
  for(i=1, j=2; i< freq_hi; i++, j+=2) {
    temp1[i]= sqrt(pow_fft[j]*pow_fft[j]+ pow_fft[j+1]*pow_fft[j+1])/freq_hi;
    if(fabs(pow_fft[j+1]) < 1e-30 && fabs(pow_fft[j]) < 1e-30)
      temp2[i]= 0.0;
    else
      temp2[i]= (180./M_PI)*atan2(pow_fft[j], pow_fft[j+1]);
  }
  for(i=0; i< freq_hi; i++) {
    pow_fft[i]= temp1[i];
    pow_fft[freq_hi+i]= temp2[i];
  }

  /******************************************************/
  /**** Computing mag and phase of the voltage signal ***/

  temp1[0]= fabs(phi_fft[0])/freq_hi/2;
  temp2[0]= (phi_fft[0] > 0.0) ? 0.0 : 180.0;
  for(i=1, j=2; i< freq_hi; i++, j+=2) {
    temp1[i]= sqrt(phi_fft[j]*phi_fft[j]+ phi_fft[j+1]*phi_fft[j+1])/freq_hi;
    if(fabs(phi_fft[j+1]) < 1e-30 && fabs(phi_fft[j]) < 1e-30)
      temp2[i]= 0.0;
    else
      temp2[i]= (180./M_PI)*atan2(phi_fft[j], phi_fft[j+1]);
  }
  for(i=0; i< freq_hi; i++) {
    phi_fft[i]= temp1[i];
    phi_fft[freq_hi+i]= temp2[i];
  }

  /************************************************************/
  /**** Computing mag and phase of the mid-potential signal ***/

  temp1[0]= fabs(mphi_fft[0])/freq_hi/2;
  temp2[0]= (mphi_fft[0] > 0.0) ? 0.0 : 180.0;
  for(i=1, j=2; i< freq_hi; i++, j+=2) {
    temp1[i]= sqrt(mphi_fft[j]*mphi_fft[j]+ mphi_fft[j+1]*mphi_fft[j+1])/freq_hi;
    if(fabs(mphi_fft[j+1]) < 1e-30 && fabs(mphi_fft[j]) < 1e-30)
      temp2[i]= 0.0;
    else
      temp2[i]= (180./M_PI)*atan2(mphi_fft[j], mphi_fft[j+1]);
  }
  for(i=0; i< freq_hi; i++) {
    mphi_fft[i]= temp1[i];
    mphi_fft[freq_hi+i]= temp2[i];
  }
}

/***************************************************************/

void velocity(void)
{
  int i, isp, index, dum1;
  static int initflag=1;
  static float dvx[NSMAX], xoffset[NSMAX],dvy[NSMAX], yoffset[NSMAX],
  dvz[NSMAX], zoffset[NSMAX], normal;

  if(initflag) {
    normal = 1;
    initflag =0;
    for(isp=0; isp<nsp; isp++) {
      dvx[isp] = (vxu[isp] - vxl[isp])/((nvxbin[isp]-1)*vscale);
      xoffset[isp]= vxl[isp]/vscale/dvx[isp];
      dvy[isp] = (vyu[isp] - vyl[isp])/((nvybin[isp]-1)*vscale);
      yoffset[isp]= vyl[isp]/vscale/dvy[isp];
      dvz[isp] = (vzu[isp] - vzl[isp])/((nvzbin[isp]-1)*vscale);
      zoffset[isp]= vzl[isp]/vscale/dvz[isp];
    }
    for(isp=0; isp<nsp;isp++)
      {
        for(i=0; i<nvxbin[isp]; i++)
          vx_array[isp][i] = vxl[isp]+i*dvx[isp]*vscale;
        for(i=0; i<nvybin[isp]; i++)
          vy_array[isp][i] = vyl[isp]+i*dvy[isp]*vscale;
        for(i=0; i<nvzbin[isp]; i++)
          vz_array[isp][i] = vzl[isp]+i*dvz[isp]*vscale;
      }
  }

  for(isp=0;isp<nsp;isp++){
    if (nvxbin[isp]){
      for(i=0; i<np[isp];i++){
        index = (int)(vx[isp][i]/dvx[isp] - xoffset[isp] +0.5);
        if((index>=0) && (index < nvxbin[isp])){
          dum1 = (int) (x[isp][i] + 0.499);
          vx_dist[isp][dum1][index] += normal;
        }
      }
    }
    if (nvybin[isp]){
      for(i=0; i<np[isp];i++){
        index = (int)(vy[isp][i]/dvy[isp] - yoffset[isp] +0.5);
        if((index>=0) && (index < nvybin[isp])){
          dum1 = (int) (x[isp][i] + 0.499);
          vy_dist[isp][dum1][index] += normal;
        }
      }
    }
    if (nvzbin[isp]){
      for(i=0; i<np[isp];i++){
        index = (int)(vz[isp][i]/dvz[isp] - zoffset[isp] +0.5);
        if((index>=0) && (index < nvzbin[isp])){
          dum1 = (int) (x[isp][i] + 0.499);
          vz_dist[isp][dum1][index] += normal;
        }
        /* note temporary hokey normalization*/
      }
    }
  }
}
