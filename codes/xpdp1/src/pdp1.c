/** \file pdp1.c
* \brief Contains main and the implementation of the main physics loop, XGMainLoop().*/

#include "pdp1.h"
#include "xgrafix.h"

void display_title(void); ///<Prints the XPDP1 title to cout.
void InitWindows(void); ///<Initializes diagnostic windows.

void main(int argc, char *argv[])
{
  display_title();        // Display XPDP1 title
  XGInit(argc, argv, &t); // Initialize XGrafix stuff

  start();                // Allocate arrays and initailze
  if(theRunWithXFlag) {   // Probably wether or not to use XGraphics, probably declared in xgrafix.h.
    mccdiag_init();       // Initialize MCC rate diagnostic
    InitWindows();        // Initialize diagnostic windows
  }
  setrho();               // Set initial charge density
  fields();               // Initialize field arrays
  if(theRunWithXFlag)
    history();            // Initialize history arrays

  XGStart();              // Start XGrafix main loop - probably calls XGMainLoop
}

/***********************************************************/
/*  The main physics loop                                  */
void XGMainLoop(void){
  register int j, isp;
  const int DiagFlag = theRunWithXFlag && n_ave; //Diagnostics are only done when graphics are turned on, and the nb of samples for diagnostics is > 0.
  //float frac;

  t += dt;

  /*no subcycling loop*/
  for(isp=0; isp< nsp; isp++) {
    it[isp]++;                      /* Advance time for the species isp         */
    (*moveptr)(isp, DiagFlag);      /* Advance position and velocity            */
    adjust(isp);                    /* Remove particles that cross boundaries   */
    (*mccptr) (isp);                /* Monte Carlo collisions for species isp   */
  }

  /* gather needs to be in a seperate loop because new particles might
     be created in mcc and adjust that might not have been weighted. */
  for(isp=0; isp< nsp; isp++) {
    gather(isp);                    /* Assign charge densities to the grid      */
    for (j=0; j<ng; j++)
      sp_n[isp][j]=sp_n_k[isp][j];
  }

  fields();

  if(theRunWithXFlag) history();
}

/***********************************************************/

void display_title(void){
  puts("\n\nXPDP1 Version 4.11");
  puts("Copyright (C) 1988-2002");
  puts("Regents of the University of California");
  puts("Plasma Theory and Simulation Group");
  puts("University of California - Berkeley\n");
}
