List of bugs, missing features, and general warnings for users of xpdp1_rfa:
	-left RFA grid should not be set to floating potential - if it is, it becomes thin (Thin grids have a lesser effective potential because their potential is seen averaged by the particle)

