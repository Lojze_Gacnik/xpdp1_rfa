\contentsline {section}{\numberline {1}\bf INTRODUCTION}{4}
\contentsline {subsection}{\numberline {1.1}\bf Scope}{4}
\contentsline {subsection}{\numberline {1.2}\bf Objectives}{5}
\contentsline {subsection}{\numberline {1.3}\bf History}{5}
\contentsline {subsection}{\numberline {1.4}\bf Enhancements}{5}
\contentsline {subsubsection}{\numberline {1.4.1}\bf Version \unhbox \voidb@x \hbox {PDW1-2.0}}{5}
\contentsline {subsubsection}{\numberline {1.4.2}\bf Version 1.0}{6}
\contentsline {subsubsection}{\numberline {1.4.3}\bf Version 2.0}{9}
\contentsline {subsubsection}{\numberline {1.4.4}\bf Version 2.1}{9}
\contentsline {subsubsection}{\numberline {1.4.5}\bf Version 3.0 (XPDP1)}{10}
\contentsline {subsubsection}{\numberline {1.4.6}\bf Version 3.1 (XPDP1)}{10}
\contentsline {subsubsection}{\numberline {1.4.7}\bf Version 4.0 (XPDP1)}{10}
\contentsline {subsubsection}{\numberline {1.4.8}\bf Version 4.1 (XPDP1)}{11}
\contentsline {section}{\numberline {2}\bf INSTALLATION}{12}
\contentsline {subsection}{\numberline {2.1}\bf Distribution Format}{12}
\contentsline {subsection}{\numberline {2.2}\bf Setup and Installation Procedure (X Windows version)}{13}
\contentsline {section}{\numberline {3}\bf X-WINDOWS PROGRAM OPERATION}{15}
\contentsline {subsection}{\numberline {3.1}\bf Syntax}{15}
\contentsline {subsection}{\numberline {3.2}\bf GUI Support}{15}
\contentsline {subsection}{\numberline {3.3}\bf Main Menu}{15}
\contentsline {subsection}{\numberline {3.4}\bf Diagnostic Window Buttons}{16}
\contentsline {subsubsection}{\numberline {3.4.1}\bf Rescale}{16}
\contentsline {subsubsection}{\numberline {3.4.2}\bf Trace}{16}
\contentsline {subsubsection}{\numberline {3.4.3}\bf Print}{16}
\contentsline {subsubsection}{\numberline {3.4.4}\bf Crosshair}{16}
\contentsline {subsection}{\numberline {3.5}\bf Diagnostics}{17}
\contentsline {section}{\numberline {4}\bf INPUT FILES}{18}
\contentsline {subsection}{\numberline {4.1}\bf Input File Parameters}{18}
\contentsline {subsubsection}{\numberline {4.1.1}\bf Global Parameters}{18}
\contentsline {subsubsection}{\numberline {4.1.2}\bf Applied Voltage Or Current Sources}{19}
\contentsline {subsubsection}{\numberline {4.1.3}\bf Flags}{21}
\contentsline {subsubsection}{\numberline {4.1.4}\bf Wall Emission Coefficients and Neutral Gas Parameters}{22}
\contentsline {subsubsection}{\numberline {4.1.5}\bf Electron-Neutral Collisional Parameters}{22}
\contentsline {subsubsection}{\numberline {4.1.6}\bf Ion-Neutral Collisional Parameters}{24}
\contentsline {subsubsection}{\numberline {4.1.7}\bf Species Parameters}{24}
\contentsline {subsection}{\numberline {4.2}\bf Input File Library}{29}
\contentsline {subsubsection}{\numberline {4.2.1}\bf PIIIA.INP (PIIIH.INP)}{30}
\contentsline {subsubsection}{\numberline {4.2.2}\bf QMACH.INP}{30}
\contentsline {subsubsection}{\numberline {4.2.3}\bf RFDA.INP (RFDH.INP)}{30}
\contentsline {subsubsection}{\numberline {4.2.4}\bf VC.INP}{31}
\contentsline {subsection}{\numberline {4.3}\bf Format}{31}
\contentsline {section}{\numberline {A}\bf SYSTEM REQUIREMENTS}{32}
\contentsline {section}{\numberline {B}\bf APPENDIX TECHNICAL SUPPORT}{33}
